# Paper Abstract :Applications of Boolean modeling to study the dynamics of a complex disease and therapeutics responses
The use of computational modeling has become a crucial tool in the investigation of complex molecular processes involved in biological systems and diseases. In this study, we explore the application of Boolean modeling in uncovering the molecular mechanisms underlying Parkinson's disease (PD). Our approach is based on the PD-map, a comprehensive molecular interaction diagram that represents key mechanisms involved in the initiation and progression of PD. By utilizing Boolean modeling, we aim to gain a deeper understanding of the disease dynamics, identify potential drug targets, and simulate the response to treatments. The results of our analysis demonstrate the effectiveness of Boolean modeling in uncovering the intricacies of PD. Our findings not only confirm existing knowledge about the disease but also provide valuable insights into the underlying mechanisms and suggest potential targets for therapeutic intervention. This study highlights the value of computational modeling in advancing our understanding of complex biological systems and diseases and highlights the importance of continued research in this field.
## Data 
The data directory includes all the data we used in the validation analysis.
## Models
The model directory includes information about the different modeling formats.
## Results
The result directory includes multiple subdirectories, showing the results of each analysis we performed from the model verification to the cohort specific Boolean simulation. This part does not include the results from model construction. The model results of the model construction are found on the models directory.
## Scripts
The script directory includes the scripts we used in data wrangling, and Boolean simulation and includes essential pre-install guidlines for some tools.
## N.B. 
- Install R, Python and packages pymaBoss, RMut, then put a relevant format of the model into the directory models and run the script
- This repository is supporting code for a paper


