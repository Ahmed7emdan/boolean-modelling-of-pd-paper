---
Title: "BoolNet SimulationT-cell diffrentiation"
By: "(Ahmed Abdelmonem Hemedan)"
date: "3/18/2020"
---
# Background
 During an immune response, CD4+ T cells differentiate into  T helper (Th1, Th2, and Th17) cells and induced regulatory (iTreg) cells based on a cytokine milieu. The resulted complex phenotypes were observed since there is a combination of classical T cell lineage.
In this study, the authors tried to identify the capacity of T cell differentiation in response to the complex combination extracellular environment. A comprehensive mechanistic (logical) computational model of the signal transduction was constructed to regulate T cell differentiation. The model’s dynamics were identified and examined under 511 different environmental conditions. Under these conditions, the model predicted the classical as well as the novel complex (mixed) T cell phenotypes that can co-express transcription factors (TFs) related to multiple differentiated T cell lineages. Analyses of the model suggest that the lineage decision is regulated by both compositions and dosage of signals that constitute the extracellular environment.
In this regard, the authors first characterized the specific patterns of extracellular environments that result in novel T cell phenotypes. Next, they predicted the inputs that can regulate the transition between the canonical and complex T cell phenotypes in a dose-dependent manner. Finally, they predicted the optimal levels of inputs that can simultaneously maximize the activity of multiple lineage-specifying TFs and that can drive a phenotype toward one of the co-expressed TFs.
This study provides new insights into the plasticity of CD4+ T cell differentiation, and also acts as a tool to design testable hypotheses for the generation of complex T cell phenotypes by various input combinations and dosages.

# Possible Cytokine Composition and Dosage represented as inputs and their phenotypes linked by TFs lineage
---

# Model description
---
The model includes 38 components (mainly proteins). It contains nine external components that represent the extracellular environment, including eight cytokines (IFN-γ, TGF-β, IL-4, IL-6, IL-12, IL-18, IL-23, and IL-27) and a generic TCR ligand.
The final model consists of 38 components (29 internal and 9 external) connected with 96 interactions.
# State Space Analysis
---
The aim is to identify cell phenotypes resulted from the differentiation of naive T cells (i.e., all model components are inactive).
For that, we identified the ergodic set using Markov simulation from BoolNet and thus has an associated limiting distribution.
Activities of the internal components are interpreted by approximating the limiting distribution of the Markov chain via simulations in BoolNet.
This means that each internal component has a unitless activity level refers to the probability that it is active in the limiting distribution of the Markov chain.
The algorithm identifies significant states by calculating the Markov matrix from the set of transition functions, where each entry of the matrix specifies the probability of a state transition from the state belonging to the corresponding row to the state belonging to the corresponding column. A vector is initialized with uniform probability for all states (or – if specified – uniform probability for all start states) and repeatedly multiplied with the Markov matrix. This returns all states with non-zero probability in this vector.
By this way, the model will arrive in one of a (probably) different ergodic sets collection. To find all the ergodic sets, one would need to let the system evolve from every possible initial condition. Given a large number of possible initial conditions (229), this is computationally infeasible. Thus, we found those ergodic sets that can be reached from the initial state where all internal components are fixed as inactive.
# Model Simulations
---
Model simulations were performed in the BoolNet and compare both synchronous and asynchronous updates. The activity states of external components are defined depending on the desired experiment by fixing the gene activity. Once the ergodic sets were identified, expressions of the internal components and their dependencies on the states of the external cytokines and the TCR ligand were investigated
then we choose their key states as an initial condition and then simulated the model to perturb with the corresponding extracellular conditions via the BoolNet.

# Results(Complex and simple phenotypes)
---
| Phenotype                                   | Co-expressed TFs                       | Maximum input composition                                         | Minimum input composition                                        |
|---------------------------------------------|----------------------------------------|-------------------------------------------------------------------|------------------------------------------------------------------|
| Th1                                         | Tbet                                   | TCR + IL-18+IL-27+IFN-γ+IL-4+IL-6+IL-23                           | TCR+IL-12/IL-27/IFN-γ                                            |
| Th2                                         | GATA3                                  | TCR+ TGF-β + IL-23 + IL-18 (or IL12) + IL-4+IL-6                  | TCR + IL-4                                                       |
| iTreg                                       | Foxp3                                  | TCR + TGF-β + IL-23 + IL-18                                       | TCR + TGF-β                                                      |
| Th17-iTreg                                  | Foxp3-RORγt                            | TCR + TGF-β + IL-6 + IL-18 + IL-23                                | TCR + TGF-β + IL-6                                               |
| Th1-Th2                                     | Tbet-GATA3                             | TCR + IFN-γ + IL-12 + IL-18 + IL-23 + IL-27 + IL-4 + IL-6         | TCR + IFN-γ/IL-12/IL-27 + IL-4TCR + IL-12 + IL-18                |
| Th1-iTreg                                   | Tbet-Foxp3                             | TCR + IFN-γ + IL-12 + IL-23 + IL-27 + TGF-β                       | TCR + IFN-γ/IL-12/ IL-27 + TGF-β                                 |
| TCR + IFN-γ + IL-18 +IL-23 + IL-27 + TGF-β  | TCR + IFN-γ/IL-12/ IL-27 + TGF-β       |                                                                   |                                                                  |
| Th1-Th17-iTreg                              | Tbet-Foxp3-RORγt                       | TCR + IFN-γ + IL-12 + IL-23 + IL-27 + IL-6 + TGF-β                | TCR+ IFN-γ/IL-12/ IL-27 + IL-6 + TGF-β                           |
| TCR + IFN-γ + IL-18 + IL-23 + IL-27 + TGF-β | TCR+ IFN-γ/IL-12/ IL-27 + IL-6 + TGF-β |                                                                   |                                                                  |
| Th1-Th2-iTreg                               | Tbet-GATA3-Foxp3                       | TCR + IFN-γ + IL-12 + IL-18 + IL-23 + IL-27 + IL-4 + TGF-β        | TCR + IFNγ / IL12 / IL27 + IL4 + TGFβ                            |
| Th1-Th2-Th17-iTreg                          | Tbet-GATA3-RORγt-Foxp3                 | TCR + IFN-γ + IL-12 + IL-18 + IL-23 + IL-27 + IL-4 + IL-6 + TGF-β | TCR + IFN-γ / IL-12 /IL-27 + IL-18 + IL-23 + IL-4 + IL-6 + TGF-β |


| Presence (1) and Absence (0) of inputs |       |       |       |      |       |      |       |     | Active TF(s)     | Phenotype         |
|----------------------------------------|-------|-------|-------|------|-------|------|-------|-----|------------------|-------------------|
| TGF-β                                  | IL-23 | IL-12 | IL-18 | IL-4 | IL-27 | IL-6 | IFN-γ | TCR |                  |                   |
| 1                                      | 1     | 1     | 1     | 1    | 0     | 1    | 0     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 1     | 0     | 0     | 1    | 0     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 0     | 1     | 1     | 1    | 1     | 1    | 0     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 0     | 1     | 1     | 1    | 0     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 0     | 1     | 1     | 1    | 0     | 1    | 0     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 0     | 1     | 1     | 0    | 1     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 0     | 1     | 1     | 0    | 1     | 1    | 0     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 0     | 1     | 1     | 0    | 0     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 0     | 1     | 1     | 0    | 0     | 1    | 0     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 0     | 1     | 0     | 1    | 1     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 0     | 1     | 0     | 1    | 1     | 1    | 0     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 0     | 1     | 0     | 1    | 0     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 1     | 1     | 1     | 0    | 1     | 1    | 0     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 0     | 0     | 1     | 1    | 1     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 0     | 0     | 1     | 1    | 1     | 1    | 0     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 0     | 0     | 1     | 1    | 0     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 0     | 0     | 0     | 1    | 1     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 0     | 0     | 0     | 1    | 1     | 1    | 0     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 0     | 0     | 0     | 1    | 0     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 1     | 1     | 1     | 0    | 0     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 1     | 1     | 1     | 0    | 0     | 1    | 0     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 1     | 1     | 0     | 1    | 1     | 1    | 0     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 1     | 1     | 0     | 1    | 0     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 1     | 0     | 1     | 1    | 1     | 1    | 0     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 1     | 0     | 1     | 1    | 0     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 1     | 0     | 0     | 1    | 1     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 1     | 0     | 0     | 1    | 1     | 1    | 0     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 0     | 1     | 1     | 1    | 1     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 1     | 1     | 1     | 0    | 1     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 1     | 1     | 1     | 1    | 1     | 1    | 0     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 1     | 1     | 0     | 1    | 1     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 1     | 0     | 1     | 1    | 1     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 1     | 1     | 1     | 1    | 0     | 1    | 1     | 1   | ALL ACTIVE       | Th1-Th2-Th17-Treg |
| 1                                      | 1     | 0     | 0     | 0    | 0     | 0    | 0     | 1   | Foxp3            | Treg              |
| 1                                      | 0     | 0     | 1     | 0    | 0     | 0    | 0     | 1   | Foxp3            | Treg              |
| 1                                      | 0     | 0     | 0     | 0    | 0     | 0    | 0     | 1   | Foxp3            | Treg              |
| 1                                      | 1     | 0     | 1     | 0    | 0     | 0    | 0     | 1   | Foxp3            | Treg              |
| 1                                      | 1     | 0     | 0     | 1    | 0     | 1    | 0     | 1   | GATA3            | Th2               |
| 1                                      | 1     | 0     | 0     | 1    | 0     | 1    | 0     | 1   | GATA3            | Th2               |
| 1                                      | 1     | 0     | 0     | 1    | 0     | 0    | 0     | 1   | GATA3            | Th2               |
| 1                                      | 0     | 1     | 0     | 1    | 0     | 1    | 0     | 1   | GATA3            | Th2               |
| 1                                      | 0     | 1     | 0     | 1    | 0     | 1    | 0     | 1   | GATA3            | Th2               |
| 1                                      | 0     | 1     | 0     | 1    | 0     | 0    | 0     | 1   | GATA3            | Th2               |
| 1                                      | 0     | 0     | 1     | 1    | 0     | 1    | 0     | 1   | GATA3            | Th2               |
| 1                                      | 0     | 0     | 1     | 1    | 0     | 1    | 0     | 1   | GATA3            | Th2               |
| 1                                      | 0     | 0     | 1     | 1    | 0     | 0    | 0     | 1   | GATA3            | Th2               |
| 1                                      | 0     | 0     | 0     | 1    | 0     | 1    | 0     | 1   | GATA3            | Th2               |
| 1                                      | 0     | 0     | 0     | 1    | 0     | 1    | 0     | 1   | GATA3            | Th2               |
| 1                                      | 0     | 0     | 0     | 1    | 0     | 0    | 0     | 1   | GATA3            | Th2               |
| 0                                      | 1     | 1     | 0     | 1    | 0     | 1    | 0     | 1   | GATA3            | Th2               |
| 0                                      | 1     | 1     | 0     | 1    | 0     | 0    | 0     | 1   | GATA3            | Th2               |
| 0                                      | 1     | 0     | 1     | 1    | 0     | 1    | 0     | 1   | GATA3            | Th2               |
| 0                                      | 1     | 0     | 1     | 1    | 0     | 0    | 0     | 1   | GATA3            | Th2               |
| 0                                      | 1     | 0     | 0     | 1    | 0     | 1    | 0     | 1   | GATA3            | Th2               |
| 0                                      | 1     | 0     | 0     | 1    | 0     | 0    | 0     | 1   | GATA3            | Th2               |
| 1                                      | 1     | 1     | 0     | 1    | 0     | 1    | 0     | 1   | GATA3            | Th2               |
| 1                                      | 1     | 1     | 0     | 1    | 0     | 1    | 0     | 1   | GATA3            | Th2               |
| 0                                      | 0     | 1     | 0     | 1    | 0     | 1    | 0     | 1   | GATA3            | Th2               |
| 0                                      | 0     | 1     | 0     | 1    | 0     | 0    | 0     | 1   | GATA3            | Th2               |
| 0                                      | 0     | 0     | 1     | 1    | 0     | 1    | 0     | 1   | GATA3            | Th2               |
| 1                                      | 1     | 1     | 0     | 1    | 0     | 0    | 0     | 1   | GATA3            | Th2               |
| 0                                      | 0     | 0     | 1     | 1    | 0     | 0    | 0     | 1   | GATA3            | Th2               |
| 0                                      | 0     | 0     | 0     | 1    | 0     | 1    | 0     | 1   | GATA3            | Th2               |
| 0                                      | 0     | 0     | 0     | 1    | 0     | 0    | 0     | 1   | GATA3            | Th2               |
| 1                                      | 1     | 0     | 1     | 1    | 0     | 1    | 0     | 1   | GATA3            | Th2               |
| 1                                      | 1     | 0     | 1     | 1    | 0     | 1    | 0     | 1   | GATA3            | Th2               |
| 1                                      | 1     | 0     | 1     | 1    | 0     | 0    | 0     | 1   | GATA3            | Th2               |
| 1                                      | 1     | 0     | 0     | 1    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 0     | 1    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 0     | 1    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 0     | 1    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 0     | 1    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 1     | 1    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 0     | 1    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 0     | 0    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 0     | 0    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 0     | 0    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 0     | 0    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 1     | 1    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 1     | 1    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 1     | 1    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 1     | 1    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 1     | 1    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 1     | 1    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 1     | 1    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 1     | 1    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 1     | 1    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 1     | 0    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 1     | 0    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 1     | 0    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 1     | 1    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 1     | 0    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 1     | 0    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 1     | 0    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 1     | 0    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 1     | 0    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 0     | 1    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 0     | 1    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 0     | 1    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 0     | 1    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 0     | 1    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 1     | 0    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 0     | 1    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 0     | 1    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 0     | 1    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 0     | 0    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 0     | 0    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 0     | 0    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 1     | 0     | 0    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 1     | 0    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 1     | 1    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 1     | 1    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 1     | 1    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 1     | 1    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 1     | 1    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 1     | 1    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 1     | 1    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 1     | 1    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 1     | 0    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 1     | 0    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 1     | 0    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 1     | 0    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 1     | 0    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 0     | 1    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 0     | 1    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 0     | 1    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 1     | 0    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 0     | 1    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 0     | 1    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 0     | 1    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 0     | 1    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 0     | 1    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 0     | 0    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 0     | 0    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 0     | 0    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 1     | 0    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 0     | 0     | 0     | 0    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 1     | 1    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 1     | 1    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 1     | 1    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 1     | 1    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 1     | 1    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 1     | 1    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 1     | 1    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 1     | 0    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 1     | 1    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 1     | 0    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 1     | 0    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 1     | 0    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 1     | 0    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 1     | 0    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 1     | 0    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 1     | 0    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 1     | 0    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 0     | 1    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 1     | 0    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 0     | 1    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 0     | 1    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 0     | 1    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 0     | 1    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 0     | 1    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 1     | 1    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 0     | 1    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 0     | 1    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 0     | 0    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 0     | 0    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 1     | 0    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 0     | 0    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 1     | 0     | 0    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 1     | 1    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 1     | 1    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 1     | 1    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 1     | 1    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 1     | 1    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 0     | 1    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 1     | 1    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 1     | 1    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 1     | 1    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 1     | 0    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 1     | 0    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 1     | 0    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 1     | 0    | 0     | 1    | 0     | 1   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 1     | 0    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 0     | 1    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 1     | 0    | 0     | 0    | 0     | 1   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 0     | 1    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 0     | 1    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 0     | 1    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 0     | 1    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 0     | 1    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 0     | 1    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 0     | 1    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 0     | 1    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 0     | 0    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 0     | 1    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 0     | 0    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 0     | 0    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 0     | 0    | 0     | 1    | 0     | 1   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 0     | 0    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 1     | 0     | 0     | 0    | 0     | 0    | 0     | 1   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 1     | 1    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 1     | 1    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 1     | 1    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 0     | 1    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 1     | 1    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 1     | 1    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 1     | 1    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 1     | 1    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 1     | 1    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 1     | 0    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 1     | 0    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 1     | 0    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 1     | 0    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 1     | 0    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 0     | 1    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 1     | 0    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 1     | 0    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 1     | 0    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 0     | 1    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 0     | 1    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 0     | 1    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 0     | 1    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 0     | 1    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 0     | 1    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 0     | 1    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 0     | 1    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 0     | 1    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 0     | 0    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 0     | 0    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 0     | 0    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 1     | 0     | 0    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 1     | 1    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 0     | 1    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 1     | 1    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 1     | 1    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 1     | 1    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 1     | 1    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 1     | 1    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 1     | 1    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 1     | 1    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 1     | 0    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 1     | 0    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 0     | 1    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 1     | 0    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 1     | 0    | 0     | 1    | 0     | 1   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 1     | 0    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 1     | 0    | 0     | 0    | 0     | 1   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 0     | 1    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 0     | 1    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 0     | 1    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 0     | 1    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 0     | 1    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 0     | 0    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 0     | 1    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 0     | 1    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 0     | 1    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 0     | 0    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 0     | 0    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 1     | 1    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 0     | 0    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 0     | 0    | 0     | 1    | 0     | 1   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 0     | 0    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 0     | 0    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 0                                      | 0     | 0     | 0     | 0    | 0     | 0    | 0     | 1   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 0     | 0    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 0     | 0    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 0     | 0    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 0     | 0    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 1     | 1    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 1     | 1    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 1     | 1    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 1     | 1    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 1     | 1    | 1     | 0    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 1     | 1    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 1     | 1    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 1     | 1    | 0     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 1     | 1    | 0     | 0    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 1     | 0    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 1     | 0    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 1     | 0    | 1     | 0    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 1     | 0    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 1     | 1    | 0     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 1     | 0    | 0     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 0     | 1    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 0     | 1    | 1     | 1    | 0     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 1     | 1     | 1    | 1     | 1    | 1     | 0   | NO TF            | Th0               |
| 1                                      | 1     | 0     | 0     | 0    | 0     | 1    | 0     | 1   | RORγt-Foxp3      | Th17-Treg         |
| 1                                      | 0     | 0     | 1     | 0    | 0     | 1    | 0     | 1   | RORγt-Foxp3      | Th17-Treg         |
| 1                                      | 0     | 0     | 0     | 0    | 0     | 1    | 0     | 1   | RORγt-Foxp3      | Th17-Treg         |
| 1                                      | 1     | 0     | 1     | 0    | 0     | 1    | 0     | 1   | RORγt-Foxp3      | Th17-Treg         |
| 0                                      | 1     | 1     | 0     | 0    | 1     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 1     | 0     | 0    | 1     | 1    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 1     | 0     | 0    | 1     | 0    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 1     | 0     | 0    | 1     | 0    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 1     | 0     | 0    | 0     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 1     | 0     | 0    | 0     | 1    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 1     | 0     | 0    | 0     | 0    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 1     | 0     | 0    | 0     | 0    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 0     | 1     | 1    | 1     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 0     | 1     | 1    | 1     | 1    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 0     | 1     | 1    | 0     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 0     | 1     | 0    | 1     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 0     | 1     | 0    | 1     | 1    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 0     | 1     | 0    | 1     | 0    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 0     | 1     | 0    | 1     | 0    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 0     | 1     | 0    | 0     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 0     | 1     | 0    | 0     | 0    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 0     | 0     | 1    | 1     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 0     | 0     | 1    | 1     | 1    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 0     | 0     | 1    | 0     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 0     | 0     | 0    | 1     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 0     | 0     | 0    | 1     | 1    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 0     | 0     | 0    | 1     | 0    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 0     | 0     | 0    | 1     | 0    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 0     | 0     | 0    | 0     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 1     | 0     | 0     | 0    | 0     | 0    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 1     | 0     | 0    | 1     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 1     | 0     | 0    | 1     | 1    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 1     | 0     | 0    | 1     | 0    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 1     | 0     | 0    | 1     | 0    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 1     | 0     | 0    | 0     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 1     | 0     | 0    | 0     | 1    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 1     | 0     | 0    | 0     | 0    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 1     | 0     | 0    | 0     | 0    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 0     | 1     | 1    | 1     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 0     | 1     | 1    | 1     | 1    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 0     | 1     | 1    | 0     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 0     | 1     | 0    | 1     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 0     | 1     | 0    | 1     | 1    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 0     | 1     | 0    | 1     | 0    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 0     | 1     | 0    | 1     | 0    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 0     | 1     | 0    | 0     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 0     | 1     | 0    | 0     | 0    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 0     | 0     | 1    | 1     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 0     | 0     | 1    | 1     | 1    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 0     | 0     | 1    | 0     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 0     | 0     | 0    | 1     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 0     | 0     | 0    | 1     | 1    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 0     | 0     | 0    | 1     | 0    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 0     | 0     | 0    | 1     | 0    | 0     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 0     | 0     | 0    | 0     | 1    | 1     | 1   | Tbet             | Th1               |
| 0                                      | 0     | 0     | 0     | 0    | 0     | 0    | 1     | 1   | Tbet             | Th1               |
| 1                                      | 1     | 0     | 0     | 0    | 1     | 0    | 1     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 1     | 0     | 0     | 0    | 1     | 0    | 0     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 1     | 0     | 0     | 0    | 0     | 0    | 1     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 0     | 1     | 0     | 0    | 1     | 0    | 1     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 0     | 1     | 0     | 0    | 1     | 0    | 0     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 0     | 1     | 0     | 0    | 0     | 0    | 1     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 0     | 1     | 0     | 0    | 0     | 0    | 0     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 0     | 0     | 1     | 0    | 1     | 0    | 1     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 0     | 0     | 1     | 0    | 1     | 0    | 0     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 0     | 0     | 1     | 0    | 0     | 0    | 1     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 0     | 0     | 0     | 0    | 1     | 0    | 1     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 0     | 0     | 0     | 0    | 1     | 0    | 0     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 0     | 0     | 0     | 0    | 0     | 0    | 1     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 1     | 1     | 0     | 0    | 1     | 0    | 1     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 1     | 1     | 0     | 0    | 1     | 0    | 0     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 1     | 1     | 0     | 0    | 0     | 0    | 1     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 1     | 1     | 0     | 0    | 0     | 0    | 0     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 1     | 0     | 1     | 0    | 1     | 0    | 1     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 1     | 0     | 1     | 0    | 1     | 0    | 0     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 1                                      | 1     | 0     | 1     | 0    | 0     | 0    | 1     | 1   | Tbet-Foxp3       | Th1-Treg          |
| 0                                      | 1     | 1     | 1     | 1    | 1     | 1    | 0     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 1     | 1     | 1    | 0     | 1    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 1     | 1     | 1    | 0     | 1    | 0     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 1     | 1     | 0    | 1     | 1    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 1     | 1     | 0    | 1     | 1    | 0     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 1     | 1     | 0    | 0     | 1    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 1     | 1     | 0    | 0     | 1    | 0     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 1     | 0     | 1    | 1     | 1    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 1     | 0     | 1    | 1     | 1    | 0     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 1     | 0     | 1    | 1     | 0    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 1     | 0     | 1    | 1     | 0    | 0     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 1     | 0     | 1    | 0     | 1    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 1     | 0     | 1    | 0     | 0    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 0     | 1     | 1    | 1     | 0    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 0     | 1     | 1    | 1     | 0    | 0     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 0     | 1     | 1    | 0     | 0    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 0     | 0     | 1    | 1     | 0    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 0     | 0     | 1    | 1     | 0    | 0     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 0     | 0     | 1    | 0     | 0    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 1     | 1     | 1    | 1     | 1    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 1     | 1     | 1    | 1     | 1    | 0     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 1     | 1     | 1    | 0     | 1    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 1     | 1     | 1    | 0     | 1    | 0     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 1     | 1     | 0    | 1     | 1    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 1     | 1     | 0    | 1     | 1    | 0     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 1     | 1     | 0    | 0     | 1    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 1     | 1     | 0    | 0     | 1    | 0     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 1     | 0     | 1    | 1     | 1    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 1     | 0     | 1    | 1     | 1    | 0     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 1     | 0     | 1    | 1     | 0    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 1     | 0     | 1    | 1     | 0    | 0     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 1     | 0     | 1    | 0     | 1    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 1     | 0     | 1    | 0     | 0    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 0     | 1     | 1    | 1     | 0    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 0     | 1     | 1    | 1     | 0    | 0     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 0     | 1     | 1    | 0     | 0    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 0     | 0     | 1    | 1     | 0    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 0     | 0     | 1    | 1     | 0    | 0     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 0     | 0     | 0     | 1    | 0     | 0    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 0                                      | 1     | 1     | 1     | 1    | 1     | 1    | 1     | 1   | Tbet-GATA3       | Th1-Th2           |
| 1                                      | 1     | 0     | 0     | 1    | 1     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 1     | 0     | 0     | 1    | 1     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 1     | 0     | 0     | 1    | 0     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 1     | 1     | 1     | 1    | 0     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 0     | 1     | 1     | 1    | 1     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 0     | 1     | 1     | 1    | 1     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 1     | 1     | 1     | 1    | 0     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 0     | 1     | 1     | 1    | 0     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 0     | 1     | 1     | 1    | 0     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 0     | 1     | 1     | 0    | 1     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 0     | 1     | 1     | 0    | 1     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 0     | 1     | 1     | 0    | 0     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 0     | 1     | 1     | 0    | 0     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 0     | 1     | 0     | 1    | 1     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 0     | 1     | 0     | 1    | 1     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 0     | 1     | 0     | 1    | 0     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 0     | 0     | 1     | 1    | 1     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 0     | 0     | 1     | 1    | 1     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 1     | 1     | 1     | 0    | 1     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 0     | 0     | 1     | 1    | 0     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 1     | 1     | 1     | 0    | 1     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 0     | 0     | 0     | 1    | 1     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 0     | 0     | 0     | 1    | 1     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 0     | 0     | 0     | 1    | 0     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 0                                      | 1     | 1     | 1     | 1    | 1     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 0                                      | 1     | 1     | 1     | 1    | 1     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 0                                      | 1     | 1     | 1     | 1    | 0     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 0                                      | 1     | 1     | 1     | 1    | 0     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 0                                      | 1     | 1     | 1     | 0    | 1     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 0                                      | 1     | 1     | 1     | 0    | 1     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 1     | 1     | 1     | 0    | 0     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 0                                      | 1     | 1     | 1     | 0    | 0     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 0                                      | 1     | 1     | 1     | 0    | 0     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 1     | 1     | 1     | 0    | 0     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 1     | 1     | 0     | 1    | 1     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 1     | 1     | 0     | 1    | 1     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 0                                      | 0     | 1     | 1     | 1    | 1     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 0                                      | 0     | 1     | 1     | 1    | 1     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 0                                      | 0     | 1     | 1     | 1    | 0     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 0                                      | 0     | 1     | 1     | 1    | 0     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 0                                      | 0     | 1     | 1     | 0    | 1     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 0                                      | 0     | 1     | 1     | 0    | 1     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 0                                      | 0     | 1     | 1     | 0    | 0     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 0                                      | 0     | 1     | 1     | 0    | 0     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 1     | 1     | 0     | 1    | 0     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 1     | 1     | 1     | 1    | 1     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 1     | 0     | 1     | 1    | 1     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 1     | 0     | 1     | 1    | 1     | 0    | 0     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 1     | 0     | 1     | 1    | 0     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 1     | 1     | 1     | 1    | 1     | 0    | 1     | 1   | Tbet-GATA3-Foxp3 | Th1-Th2-Treg      |
| 1                                      | 1     | 0     | 0     | 0    | 1     | 1    | 1     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 1     | 0     | 0     | 0    | 1     | 1    | 0     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 1     | 0     | 0     | 0    | 0     | 1    | 1     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 0     | 1     | 0     | 0    | 1     | 1    | 1     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 0     | 1     | 0     | 0    | 1     | 1    | 0     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 0     | 1     | 0     | 0    | 0     | 1    | 1     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 0     | 1     | 0     | 0    | 0     | 1    | 0     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 0     | 0     | 1     | 0    | 1     | 1    | 1     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 0     | 0     | 1     | 0    | 1     | 1    | 0     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 0     | 0     | 1     | 0    | 0     | 1    | 1     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 0     | 0     | 0     | 0    | 1     | 1    | 1     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 0     | 0     | 0     | 0    | 1     | 1    | 0     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 0     | 0     | 0     | 0    | 0     | 1    | 1     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 1     | 1     | 0     | 0    | 1     | 1    | 1     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 1     | 1     | 0     | 0    | 1     | 1    | 0     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 1     | 1     | 0     | 0    | 0     | 1    | 1     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 1     | 1     | 0     | 0    | 0     | 1    | 0     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 1     | 0     | 1     | 0    | 1     | 1    | 1     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 1     | 0     | 1     | 0    | 1     | 1    | 0     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |
| 1                                      | 1     | 0     | 1     | 0    | 0     | 1    | 1     | 1   | Tbet-RORγt-Foxp3 | Th1-Th17-Treg     |

# Discussion
-----
* The minimal and maximal input compositions for each identified phenotype were simulated. The minimal composition includes a minimum number of inputs that can stimulate a phenotype. On the other hand, the maximal composition includes a maximum number of inputs that can be simultaneously active to result in the same phenotype.
* It is observed that the combination of IL-12 and IL-18 leads to the stimulation of GATA3 and Foxp3 even in the absence of IL-4 and TGF-β via an NF-κB-dependent pathway.The simulation shows that the combination of IL-18 and IL-12 could result in a Th1–Th2–iTreg complex phenotype.
* It is well known that IL-18 alone has a context-specific functional heterogeneity and can induce both Th1 and Th2 T cell phenotypes. The combination of IL-12 and IL-18 has been shown to have a synergistic effect on IFN-γ production that stimulates the Th1 phenotype.
* Using BoolNet, the dosage effect is simulated by using both minimum and the maximum number of inputs. This returned the complex Th1–Th2 phenotype based on the combination and dosage of IFN-γ, IL-12, IL-27, IL-18, IL-4, and the TCR ligand. The increased activity levels of the cytokines IFN-γ, IL-12, IL-27, and TCR ligand drive the phenotype toward Th1, whereas the IL-18 or IL-4 drive the Th2 phenotype.
* The IL-23 and IL-6 do not relate with either Tbet or GATA3.
* Under both maximal and minimal input compositions, the IL-4 had low to no correlation with Tbet.On the other hand, the IL-18 was positively correlated with GATA3 and negatively correlated with Tbet. Thus,  IL-18 may have a dominant role over IL-4 to produce Th2 phenotype under the Th1–Th2 stimulating environmental conditions.
* Identifying activity levels and their optimum level is not an option in BoolNet like cell collective. So I did not perform that on BoolNet.
* Getting attractors with different updating schemes including Markov simulation + synchronous and Asynchronous is time independent in case of single state attractor but differ on the probapility of reachability. However, the  ergodic sets represented as limit cycles differs according to the scheme but the system oscillates regularly in deterministic asynchronous and synchronous updating scheme. On the other hand the stochastic asynchronous and Markov and PBN the states oscillates irregularly showing the transtions between states in the same attractor.
