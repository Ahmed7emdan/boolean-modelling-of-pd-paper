Periods 1 and 2 Periods 3�15
FC, frontal cerebral wall OFC, orbital prefrontal cortex
DFC, dorsolateral prefrontal cortex
VFC, ventrolateral prefrontal cortex
MFC, medial prefrontal cortex
M1C, primary motor (M1) cortex
PC, parietal cerebral wall S1C, primary somatosensory (S1) cortex
IPC, posterior inferior parietal cortex
TC, temporal cerebral wall A1C, primary auditory (A1) cortex
STC, superior temporal cortex
ITC, inferior temporal cortex
OC, occipital cerebral wall V1C, primary visual (V1) cortex
HIP, hippocampal anlage HIP, hippocampus
� AMY, amygdala
VF, ventral forebrain STR, striatum
MGE, medial ganglionic eminence
LGE, lateral ganglionic eminence
CGE, caudal ganglionic eminence
DIE, diencephalon MD, mediodorsal nucleus of the thalamus
DTH, dorsal thalamus �
URL, upper (rostral) rhombic lip CBC, cerebellar cortex