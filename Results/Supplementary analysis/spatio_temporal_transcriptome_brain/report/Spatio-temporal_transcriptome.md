# Spatio-temporal transcriptome of the human brain
## Aim
* We aim to track the differential expressed genes over a three-period of time including Old, mid-age and young age that represent the onset of ageing. This will be used to compare with the resulted attractors from Wnt/PI3K-AKT signalling, especially how Both Wnt and IGF govern the ageing process.
## Methods 
* Data preprocessing
1. Defined to three age based groups
2. Data filteration
* DEA
* Mapping Ids with HGNC - HUGO Gene Nomenclature
* GSEA
* Mapping on PD map (compare the basins of attractions with the DEG)
## Result and discussion
* The result shows increased susceptibility to psychiatric and neurodegenerative disorders mainly schizophrenia, autism spectrum disorders (ASD) and depression.
* Of signalling pathways, there is only one related to Parkinson's p38MAPK cascade  in histamine signalling pathway and pi3k-Akt signalling particularly AGO2 which significantly co-expressed with IGFR1 [PMID: 31827079](https://www.ncbi.nlm.nih.gov/pubmed/31827079).
*  There was no relation between the resulted diffrentially expressed genes and Wnt protein related genes over different stages
* Therefore, I searched to other transcriptome datasets (https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE74247) and (https://www.ncbi.nlm.nih.gov/geo/geo2r/?acc=GSE42966) of the developing brain in Parkinson's disease

* In mouse models during ageing deregulation of KLOTHO happen which is known as age suppressing gene and at the same time it is Wnt antagonist so wnt increase coincidence with premature ageing [PMID: 17690294](https://www.ncbi.nlm.nih.gov/pubmed/17690294)
* Analysis Moran dataset (PMID: 17211632)[https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE8397] show ret proto-oncogene in Wnt signalling is diffrentially expressed 
in old ages and that gene share the downstream targets of IGF1 in PI3K and AKT pathways according to PD-map. Therefore according to (PMID: 29515352)[https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5826217/]inhibition of Akt activates GSK3β 
through reducing GSK3β phosphorylation at serine 9, whereas activation of Akt inhibits GSK3β through enhancing phosphorylation. 
Accordingly, the negative regulation of GSK3β by Akt exerts a crucial function in cell cycle progression and metabolism in PD