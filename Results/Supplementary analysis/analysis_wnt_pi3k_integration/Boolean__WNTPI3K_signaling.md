---
Title: "Boolean analysis of the  WNT/PI3K signaling"
By: "(Ahmed Abdelmonem Hemedan)"
date: "4/2/2020"
---
# A Boolean model of the WNT/PI3K signaling from PD map
# Aim
This work aimed to create a Boolean  model, which describes the  WNT/PI3K signaling as it was observed in the brain. The knowledge incorporated into this model originates from disease map . The model was then used to simulate the interactions in the context of ageing.
# Method
* Eliminate irrelevant variables from the inputs of the transition functions this is usful if the network perturbed randomly
* Asynchronous updating scheme return random set of attractors representing different states of phenotypes
* Perturb the state trajectories of a network and assesses the robustness by comparing the successor states or the attractors of a set of initial states and a set of perturbed copies of these initial states

# Result (see the complete result the attractor file)
---
| id      | name                              | young attractor | young/midaged | young/midaged | mid-aged attractor | mid aged attractor | mid aged/aged | mid aged/aged | mid aged/aged | mid aged/aged | Aged |
|---------|-----------------------------------|-----------------|---------------|---------------|--------------------|--------------------|---------------|---------------|---------------|---------------|------|
| csa1476 | TF CHOP:FOXO                      | 1               | 1             | 1             | 0                  | 1                  | 1             | 1             | 1             | 1             | 1    |
| csa1479 | TF AP-1                           | 0               | 0             | 0             | 0                  | 0                  | 0             | 0             | 1             | 0             | 0    |
| csa1500 | AMPK                              | 0               | 1             | 0             | 1                  | 1                  | 0             | 1             | 0             | 0             | 0    |
| csa1501 | TF FOXO3                          | 1               | 1             | 0             | 1                  | 1                  | 1             | 1             | 1             | 1             | 1    |
| csa1502 | TF FOXO3                          | 1               | 1             | 1             | 1                  | 1                  | 1             | 1             | 1             | 1             | 1    |

# Discussion
* In PI3k/Akt, The overexpression of LRRK2 G2019S mutant increases autophagy process [PMID: 22773119](https://www.ncbi.nlm.nih.gov/pubmed/22773119) which can be also up-regulated by inhibiting either mTORC1 or 2 or both suggesting targets to induce the autophagy [PMID: 28122627](https://www.ncbi.nlm.nih.gov/pubmed/28122627).
* Amino acids as a physiological signal came from AMPK and autophagy process itself can activate mTORC1 that regulate autophagy process by inhibition supporting the ideas and models proposed to explain the interrelationship between amino acid sensing, mTORC1 signalling and autophagy [PMID: 29233869](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5869864/). Some studies suggest inhibition mtorc1 by rapamycin is effective [PMID: 30744070](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6387269/).Genetic inactivation of mTORC1 or mTORC2 in neurons reveals distinct functions in glutamatergic synaptic transmission
[PMID: 32125271](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7080408/)
 * Inside the mitochondria Foxo3 does not show activity on protein aggregation and does not promote the autophagy in foxo3 activity compartment however in pi3k signalling as an intracellular signalling pathway, It is more affected from the foxo3 came from AMPK  more than the source of foxo3 in Lewy body so it may be important to target foxo3 for autophagy upregulation. Apart from that RNA mediated mechanisms including BECN1/GABARAPL1/MAP1LC3A/BNIP3/ATG12/MUL1 can control the autophagy process.
 * DDIT3 induced apoptosis, was found to regulate both BCL2L11 and BBC3 expression [MID: 30744070](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6387269/). DDIT3 knockdown maintains foxo3 activity. So DDIT3 may consider a relevant therapeutic target.
* Wnt signals activate  CTNNB1.  GSK3B phosphorylate the amino-terminal region of CTNNB1, resulting in CTNNB1 ubiquitination and proteasomal degradation. IGF1 initiate PI3K and then Akt is activated in response to PI3K signalling. 
* IGF1 knockout reduced phosphorylation at site T on IRS1. 