# by Ahmed Hemedan
## PBN simulation of Boolean network with MaBoSS
import maboss

### The input model is a simple Boolean network created in bnd format with its config file. 
model = maboss.load("model.bnd", "model.cfg")

### Building the `Simulation` object
# Other model formats can be converted to MaBoSS using the `to_maboss` function:
masim = ginsim.to_maboss(lrg)

# specify some nodes as "internal": the value of internal nodes is hidden in the output of simulations. 
masim.network['entity_name'].is_internal = True

# change  parameters with the function update_parameters:
masim.update_parameters(time_tick=0.1, max_time=4)

### Performing simulations
#  call MaBoSS to perfom the requested simulations. A result object is then returned.
res = masim.run()

### Result visualizations
# The result object provides access to the simulation output, and plotting functions.
res = simulation.run()

# display the mean probability of each state (composed of non-internal nodes) along time:
res.plot_trajectory()

### Simulating mutations
# Modify the network model to perform the mutation of a node, that is forcing its value to be always 1 ("ON") or always 0 ("OFF").
#  copy the initial model and configure a gain of function mutant for the node `BDNF`:
mutsim = masim.copy()

mutsim.mutate("BDNF", "OFF")

# The resulting model can then be simulated:
mutres = mutsim.run()
