----
#This code is to generate the dot file (structure of the network)
#and export a network for analysis 
# By Ahmed Hemedan
----


# import packages
import pandas as pd

'''
Generate ISPL files
Logic operations in the inputfile: &, |, ! or ~
Example of input file:
    targets, factors
    x1, x1 & x2
    x2, x1 | (!x2)
In the input file of the Boolean functions, the first role is "targets, functions". 
This row will be removed from the list of functions and nodes. 
The following code does not add brackets to negations. It's recommended to add brackets to 
surround negations manually. 
For instance, 'x1 & !(x1|x3)' should be format as 'x1 & (!(x1|x3))'
'''
def bf2ispl(bf_file, ispl_file):
    # read file and save it as a dataframe
    bfs = pd.read_csv(bf_file, sep=',', header=None, names=['nodes', 'functions'])
    if 'targets' in bfs['nodes'].iloc[0] and 'factors' in bfs['functions'].iloc[0]:
        bfs = bfs.iloc[1:]
    # save input nodes for initial state setting.
    zero_nodes = []
    one_nodes = []
    # format the nodes and functions
    for index, row in bfs.iterrows():
        f = str(row['functions'])
        n = row['nodes']
        f = f.replace(' ', '')

        if '!' in f:
            f = f.replace('!', '~')
        elif f == '0':
            zero_nodes.append(row['nodes'])
        elif f == '1':
            one_nodes.append(row['nodes'])
        row['functions'] = f

    # start to write the ispl file
    g = open(ispl_file, 'w+')
    g.write('Agent M\n\tVars:\n')
    for index, row in bfs.iterrows():
        g.write('\t\t' + row['nodes'] + ': boolean;\n')
    g.write('\tend Vars\n')
    g.write('\tActions = {none};\n\tProtocol:\n\t\tOther: {none};\n\tend Protocol\n')

    g.write('\tEvolution:\n')
    for index, row in bfs.iterrows():
        if row['nodes'] not in zero_nodes and row['nodes'] not in one_nodes:  # no input nodes
            g.write('\t\t' + row['nodes'] + '=true if ' + str(row['functions']) + '=true;\n')
            g.write('\t\t' + row['nodes'] + '=false if ' + str(row['functions']) + '=false;\n')
        elif row['nodes'] in zero_nodes or row['nodes'] in one_nodes:
            g.write('\t\t' + row['nodes'] + '=true if ' + row['nodes'] + '=true;\n')
            g.write('\t\t' + row['nodes'] + '=false if ' + row['nodes'] + '=false;\n')
    g.write('\tend Evolution\nend Agent\n\n')

    g.write('InitStates\n\t\t')
    zero_state = ['M.' + node + '=false' for node in zero_nodes]
    one_state = ['M.' + node + '=true' for node in one_nodes]
    zero_state = ' and '.join(zero_state)
    one_state = ' and '.join(one_state)
    if zero_state == '' and one_state == '':
        random_node = bfs['nodes'].iloc[0]
        ini_state = 'M.' + random_node + '=true or M.' + random_node + '=false'
    elif zero_state == '' or one_state == '':
        ini_state = zero_state + one_state
    else:
        ini_state = zero_state + ' and ' + one_state
    g.write(ini_state + ';\n')
    g.write('end InitStates')
    g.close()


def write_toDot(edges, dot_file):
    df = open(dot_file, 'w+')
    df.write('digraph dd{\n')
    for id, row in edges.iterrows():
        arrow = row['from'] + '-> ' + row['to']
        df.write('\t' + arrow + '\n')
    df.write('}')


def bf2Str(bf_file):
    # read file and save it as a dataframe
    bfs = pd.read_csv(bf_file, sep=',', header=None, names=['nodes', 'functions'])
    if 'targets' in bfs['nodes'].iloc[0] and 'factors' in bfs['functions'].iloc[0]:
        bfs = bfs.iloc[1:]
    nodes = bfs['nodes'].values.tolist()
    edges = pd.DataFrame(columns=['from', 'to', 'interaction'])
    for index, row in bfs.iterrows():
        f = str(row['functions'])
        to_node = row['nodes']
        f = f.replace(' ', '')

        if f == '0' or f == '1':
            f = to_node
        f = f.replace('&', ' ')
        f = f.replace('|', ' ')
        f = f.replace('(', '').replace(')', '')
        f = f.replace('!', '')
        f = f.split()
        for fn in f:
            edges = edges.append({'from': fn, 'to': to_node}, ignore_index=True)
        edges = edges.drop_duplicates()
    return nodes, edges


if __name__ == '__main__':
    '''set the file names'''
    file_functions = 'functions.txt'
    file_ispl = 'input.ispl'
    '''generate ispl files from Boolean functions'''
    bf2ispl(file_functions, file_ispl)

    '''
    generate the dot file (structure of the network from function)
    '''
    node_edges = bf2Str(file_functions)
    write_toDot(edges,"structure.dot")
