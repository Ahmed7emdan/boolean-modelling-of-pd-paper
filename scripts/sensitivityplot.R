
library(ggplot2)
library(cowplot)
library(reshape2)

d <- read.csv('sensitivities_knockouts.csv')

#clean
for(i in seq_len(nrow(d)-1)) {
  if(d$Pathways[i+1] == "")
    d$Pathways[i+1] <- d$Pathways[i]
}

d <- melt(d, id.vars=c('Pathways', 'GroupID'), variable.name="Measure", value.name="Distance")

ggsave("sensitiviies_knockouts.pdf", units='cm', width=15, height=20,
ggplot(d,aes(Distance, GroupID)) +
  geom_col(fill='orange') +
  ylab("") +
  facet_grid(Pathways~Measure, scales="free") +
  theme_cowplot(font_size=8) +
  theme(panel.grid.major.x = element_line(color="#eeeeee"))
)
